# 宠物服务平台

#### 介绍
一个简单的springboot项目，可做为毕业设计，内容包括宠物用品商店，宠物医院以及服务预约。技术栈包括springboot、layui、mysql、uni-app等等。文章最后有完整前端后台源码获取方式，请耐心看完。

#### 应用管理端页面截图
##### 管理端登录页面
![管理端登录页面](https://images.gitee.com/uploads/images/2021/1021/110935_b131bc26_2049205.png "屏幕截图.png")
##### 管理端注册页面
![管理端注册页面](https://images.gitee.com/uploads/images/2021/1021/111104_8b4dd5a0_2049205.png "屏幕截图.png")
##### 管理端主页面
![管理端主页面](https://images.gitee.com/uploads/images/2021/1021/111140_feabfda6_2049205.png "屏幕截图.png")
##### 上品列表页
![上品列表页](https://images.gitee.com/uploads/images/2021/1021/111224_fe476bbc_2049205.png "屏幕截图.png")
##### 商品新增页面
![商品新增页面](https://images.gitee.com/uploads/images/2021/1021/111254_ba1e80a4_2049205.png "屏幕截图.png")
##### 医生管理页面
![医生管理页面](https://images.gitee.com/uploads/images/2021/1021/142522_fe79c272_2049205.png "屏幕截图.png")
由于管理端页面过多，此处就不全部展示，但是大家可以从截图中看出管理端有哪些菜单。
#### 应用APP页面截图
##### app主页推荐商品
![app主页推荐商品](https://images.gitee.com/uploads/images/2021/1021/112802_926e90a0_2049205.jpeg "Screenshot_20211021_112005_com.hj.petHome.jpg")
##### 主页推荐医院
![主页推荐医院](https://images.gitee.com/uploads/images/2021/1021/112845_4efd4ae8_2049205.jpeg "Screenshot_20211021_112013_com.hj.petHome_edit_76.jpg")
##### 主页推荐宠物百科
![主页推荐宠物百科](https://images.gitee.com/uploads/images/2021/1021/112919_b007be99_2049205.jpeg "Screenshot_20211021_112017_com.hj.petHome.jpg")
##### 商品列表页
![商品列表页](https://images.gitee.com/uploads/images/2021/1021/112946_9a545710_2049205.jpeg "Screenshot_20211021_112029_com.hj.petHome.jpg")
##### 服务列表页
![服务列表页](https://images.gitee.com/uploads/images/2021/1021/113012_8019c0c5_2049205.jpeg "Screenshot_20211021_112033_com.hj.petHome.jpg")
##### 登录页
![登录页](https://images.gitee.com/uploads/images/2021/1021/113029_2aca1b35_2049205.jpeg "Screenshot_20211021_112039_com.hj.petHome.jpg")
##### 注册页
![注册页](https://images.gitee.com/uploads/images/2021/1021/113046_9997913c_2049205.jpeg "Screenshot_20211021_112044_com.hj.petHome.jpg")
##### 更换头像
![更换头像](https://images.gitee.com/uploads/images/2021/1021/133620_9bb5ab00_2049205.jpeg "Screenshot_20211021_112145_com.hj.petHome.jpg")
##### 个人中心
![个人中心页](https://images.gitee.com/uploads/images/2021/1021/140837_d45aff19_2049205.jpeg "Screenshot_20211021_112150_com.hj.petHome_edit_76.jpg")
##### 医院定位
![医院定位](https://images.gitee.com/uploads/images/2021/1021/133746_c58a1909_2049205.jpeg "Screenshot_20211021_112200_com.hj.petHome_edit_76.jpg")
##### 医生列表
![医生列表](https://images.gitee.com/uploads/images/2021/1021/133840_e7910db4_2049205.jpeg "Screenshot_20211021_112209_com.hj.petHome_edit_76.jpg")
##### 订单页面
![订单页面](https://images.gitee.com/uploads/images/2021/1021/133915_bc32515f_2049205.jpeg "Screenshot_20211021_112225_com.hj.petHome.jpg")
##### 购物车
![购物车](https://images.gitee.com/uploads/images/2021/1021/140906_8f7a8ec5_2049205.jpeg "Screenshot_20211021_113404_com.hj.petHome_edit_76.jpg")
### 重点
页面大致就看到这里 
需要完整源码的有两种途径：  
【闲鱼】https://m.tb.cn/h.fpujE7c?tk=IHsG27rGvyt「我在闲鱼发布了【一款用基于springboot+uniapp的宠物服务平台学】」
点击链接直接打开</b>

加QQ群：</b>

![取码群](https://images.gitee.com/uploads/images/2021/1021/134445_b6a6ca37_2049205.jpeg "qrcode_1634795050684.jpg")